<?php die(); ?>			
			
			<!DOCTYPE html>
<!--// OPEN HTML //-->
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="UTF-8">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="https://gsaraiva.com/glizigen/xmlrpc.php">
        <!-- Mobile Specific Metas
    ================================================== -->
	       <meta name="viewport" content="width=device-width, initial-scale=1.0">
    	    <title>Produtos &#8211; Glizigen</title>
                        <script>
                            /* You can add more configuration options to webfontloader by previously defining the WebFontConfig with your options */
                            if ( typeof WebFontConfig === "undefined" ) {
                                WebFontConfig = new Object();
                            }
                            WebFontConfig['google'] = {families: ['Montserrat:500,400,300,400italic,700,700italic', 'Glegoo:400,700&subset=latin']};

                            (function() {
                                var wf = document.createElement( 'script' );
                                wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js';
                                wf.type = 'text/javascript';
                                wf.async = 'true';
                                var s = document.getElementsByTagName( 'script' )[0];
                                s.parentNode.insertBefore( wf, s );
                            })();
                        </script>
                        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Feed para Glizigen &raquo;" href="https://gsaraiva.com/glizigen/feed/" />
<link rel="alternate" type="application/rss+xml" title="Feed de comentários para Glizigen &raquo;" href="https://gsaraiva.com/glizigen/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Feed de Glizigen &raquo; Produtos" href="https://gsaraiva.com/glizigen/shop/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/gsaraiva.com\/glizigen\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='dashicons-css'  href='https://gsaraiva.com/glizigen/wp-includes/css/dashicons.min.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='https://gsaraiva.com/glizigen/wp-includes/css/admin-bar.min.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.4' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.7.1' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='woocommerce-layout-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.5.0' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.5.0' type='text/css' media='all' />
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='bootstrap-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/css/bootstrap.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='line-icons-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/css/line-icons.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='material-css'  href='//fonts.googleapis.com/css?family=Material+Icons&#038;subset&#038;ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='material-icons-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/css/material-icons.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css?ver=5.4.7' type='text/css' media='all' />
<link rel='stylesheet' id='animations-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/css/animations.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-theme-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/css/bootstrap-theme.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='capital-main-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/style.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='responsive-media-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/css/responsive.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/vendor/magnific/magnific-popup.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='owl-carousel-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/vendor/owl-carousel/css/owl.carousel.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='owl-carousel2-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/vendor/owl-carousel/css/owl.theme.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='capital-colors-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/colors/color6.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='capital-custom-options-style-css'  href='https://gsaraiva.com/glizigen/wp-content/themes/capital/css/custom-option_1.css?ver=1.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='capital_dynamic_css-css'  href='https://gsaraiva.com/glizigen/wp-admin/admin-ajax.php?action=capital_dynamic_css&#038;taxp&#038;pgid=724&#038;sidebar_pos&#038;ver=4.9.8' type='text/css' media='all' />
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.7.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.7.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/glizigen\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/glizigen\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Ver carrinho","cart_url":"https:\/\/gsaraiva.com\/glizigen\/pagamento\/","is_cart":"","cart_redirect_after_add":"yes"};
/* ]]> */
</script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.5.0'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=5.4.7'></script>
<link rel='https://api.w.org/' href='https://gsaraiva.com/glizigen/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://gsaraiva.com/glizigen/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://gsaraiva.com/glizigen/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.8" />
<meta name="generator" content="WooCommerce 3.5.0" />
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
			<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<meta name="generator" content="Powered by Slider Revolution 5.4.7.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<!--// CLOSE HEAD //-->
<body class="archive post-type-archive post-type-archive-product logged-in admin-bar no-customize-support woocommerce woocommerce-page woocommerce-no-js  header-style8 wpb-js-composer js-comp-ver-5.4.7 vc_responsive">
<div class="body"> 
<div class="theme-sticky-header">
	<div class="container">
		<div class="sticky-header-left-blocks"><nav class="sticky-menu"></nav></div>		<div class="sticky-header-right-blocks"><div class="header-social-container header-equaler"><div><div>
	<ul class="header-social imi-social-icons imi-social-icons-plain imi-social-icons-large imi-social-icons-tc imi-social-icons-hover-bc">
	<li class="facebook"><a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li><li class="instagram"><a href="https://www.instagram.com" target="_blank"><i class="fa fa-instagram"></i></a></li><li class="twitter"><a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>	</ul>
</div></div></div>	<div class="featured-buttons header-equaler"><div><div>
		<a href="#" class="fbtn fbtn1 fbtn-round fbtn-md">COMPRAR AGORA</a>
	</div></div></div>
</div>	</div>
</div>
<!-- End Header --> <div class="theme-mobile-header">
	<div class="mobile-header-blocks">	<!-- Header Info -->
	<div class="header_info_text header_info_text1 header-equaler"><div><div>For help call <img src="https://capital.imithemes.com/demo3/wp-content/uploads/sites/2/2017/07/Flag_of_the_United_States.svg_.png" alt="US Flag" width="15"> <a href="tel:+080 9081 09191">+080 9081 09191</a> or <a href="mailto:info@capital.com">Email Us</a></div></div></div>

<div class="mobile-navbar">
	<div class="mobile-logo"><div><div>
					<a href="https://gsaraiva.com/glizigen/" class="default-logo"><img src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/logo-glizigen-1.png" alt="Glizigen"></a>
			<a href="https://gsaraiva.com/glizigen/" class="default-retina-logo"><img src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/logo-glizigen-1.png" alt="Glizigen" width="213" height="79"></a>
				</div></div></div>

		<div class="header-equaler"><div><div>
		<button class="mmenu-toggle"></button>	</div></div></div>
	<!-- Cloned Main Menu -->
	<nav class="main-menu-clone mobile-menu"><div><ul></ul></div></nav>
</div>	<div class="featured-buttons header-equaler"><div><div>
		<a href="#" class="fbtn fbtn1 fbtn-round fbtn-md">COMPRAR AGORA</a>
	</div></div></div>
</div></div>
<!-- End Header --> <div class="overlay-wrapper overlay-search-form-wrapper">
		<a href="#" class="overlay-wrapper-close"><i class="mi mi-close"></i></a><div><div><div class="container">   
<form method="get" class="imi-searchform" action="https://gsaraiva.com/glizigen/">
    <input type="text" name="s" id="search-form-5bd252e35153f" value="" placeholder="Search">
	<button type ="submit" name ="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
</form>
</div></div></div></div>	<div class="topper-container widgets-at-right">
		<div class="container">
			<div class="vc_row wpb_row vc_row-fluid vc_custom_1500661000382 vc_column-gap-35"><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill"><div class="vc_column-inner vc_custom_1500647861520"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="1280" height="759" src="https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/sydney2.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/sydney2.png 1280w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/sydney2-300x178.png 300w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/sydney2-768x455.png 768w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/sydney2-1024x607.png 1024w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/sydney2-958x568.png 958w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/sydney2-600x356.png 600w" sizes="(max-width: 1280px) 100vw, 1280px" /></div>
		</figure>
	</div>
<h3 style="color: #ffffff;text-align: left" class="vc_custom_heading" >CAPITAL CORP. SYDNEY</h3>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><span style="color: #fffbfb">73 Ocean Street, New South Wales 2000, SYDNEY</span></p>
<p><span style="color: #fffbfb">Contact Person: Callum S Ansell</span><br />
<span style="color: #fffbfb">E: callum.aus@capital.com</span><br />
<span style="color: #fffbfb">P: (02) 8252 5319</span></p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill"><div class="vc_column-inner vc_custom_1500660808184"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="1280" height="759" src="https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/london.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/london.png 1280w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/london-300x178.png 300w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/london-768x455.png 768w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/london-1024x607.png 1024w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/london-958x568.png 958w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/london-600x356.png 600w" sizes="(max-width: 1280px) 100vw, 1280px" /></div>
		</figure>
	</div>
<h3 style="color: #ffffff;text-align: left" class="vc_custom_heading" >WILD KEY CAPITAL</h3>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><span style="color: #f1fbff">22 Guild Street, NW8 2UP,<br />
LONDON</span></p>
<p><span style="color: #f1fbff">Contact Person: Matilda O Dunn</span><br />
<span style="color: #f1fbff">E: matilda.uk@capital.com</span><br />
<span style="color: #f1fbff">P: 070 8652 7276</span></p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill"><div class="vc_column-inner vc_custom_1500660856016"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="1280" height="759" src="https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/berlin1.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/berlin1.png 1280w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/berlin1-300x178.png 300w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/berlin1-768x455.png 768w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/berlin1-1024x607.png 1024w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/berlin1-958x568.png 958w, https://gsaraiva.com/glizigen/wp-content/uploads/2017/07/berlin1-600x356.png 600w" sizes="(max-width: 1280px) 100vw, 1280px" /></div>
		</figure>
	</div>
<h3 style="color: #ffffff;text-align: left" class="vc_custom_heading" >LECHMERE CAPITAL</h3>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p><span style="color: #fff5e2">Genslerstraße 9, Berlin Schöneberg 10829, BERLIN</span></p>
<p><span style="color: #fff5e2">Contact Person: Thorsten S Kohl</span><br />
<span style="color: #fff5e2">E: thorsten.bl@capital.com</span><br />
<span style="color: #fff5e2">P: 030 62 91 92</span></p>

		</div>
	</div>
</div></div></div></div>
		</div>
	</div>
<header class="site-header">
	<div class="container relative-container">
      	<div class="header-left-blocks"><div class="header-equaler"><div><div>
	<div class="site-logo"><div>
					<a href="https://gsaraiva.com/glizigen/" class="default-logo"><img src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/logo-glizigen-1.png" alt="Glizigen"></a>
			<a href="https://gsaraiva.com/glizigen/" class="default-retina-logo"><img src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/logo-glizigen-1.png" alt="Glizigen" width="213" height="79"></a>
			</div></div>
	<div class="sticky-logo">
					<a href="https://gsaraiva.com/glizigen/" class="default-logo"><img src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/logo-glizigen-1.png" alt="Glizigen"></a>
			<a href="https://gsaraiva.com/glizigen/" class="default-retina-logo"><img src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/logo-glizigen-1.png" alt="Glizigen" width="" height=""></a>
			</div>
</div></div></div><div class="header-equaler header-topper-opener"><div><div>
	<a href="#" class="topper-opener widgets-at-right-opener"><i class="mi mi-place"></i></a>
</div></div></div>
</div>		<div class="header-right-blocks"><ul id="menu-home" class="sf-menu dd-menu dd-style3"><li id="menu-item-654" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#" data-id="654" data-slug="home" data-home-url="">Home</a></li>
<li id="menu-item-655" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#" data-id="655" data-slug="acoes-glizigen" data-home-url="">Ações Glizigen</a></li>
<li id="menu-item-656" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#" data-id="656" data-slug="galeria-de-fotos" data-home-url="">Galeria de Fotos</a></li>
<li id="menu-item-657" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#" data-id="657" data-slug="acesso-medico" data-home-url="">Acesso Médico</a></li>
</ul><div class="header-social-container header-equaler"><div><div>
	<ul class="header-social imi-social-icons imi-social-icons-plain imi-social-icons-large imi-social-icons-tc imi-social-icons-hover-bc">
	<li class="facebook"><a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li><li class="instagram"><a href="https://www.instagram.com" target="_blank"><i class="fa fa-instagram"></i></a></li><li class="twitter"><a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>	</ul>
</div></div></div></div>	</div>
</header>
<!-- End Header -->  <div class="hero-area">
 <div class="page-banner" style="background-color:; height:130px;">
	 <div class="container">
            	<div class="page-banner-text"><div style="height:130px;"><div>        			<h1> </h1>
                                    </div></div></div>
            </div>
        </div>
    </div>
    <!-- Start Body Content -->
<div id="main-container">
  	<div class="content">
   		<div class="container">
       		<div class="row">
            	<div class="col-md-9" id="content-col">
            		
			
				<h1 class="page-title">Shop</h1>

			
			
			
				<div class="woocommerce-notices-wrapper"></div><p class="woocommerce-result-count">
	Mostrando todos os 3 resultados</p>
<form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby">
					<option value="popularity"  selected='selected'>Ordenar por popularidade</option>
					<option value="rating" >Ordenar por média de classificação</option>
					<option value="date" >Sort by latest</option>
					<option value="price" >Ordenar por preço: menor para maior</option>
					<option value="price-desc" >Ordenar por preço: maior para menor</option>
			</select>
	<input type="hidden" name="paged" value="1" />
	</form>

				<ul class="products columns-4">

																					<li class="post-724 product type-product status-publish has-post-thumbnail product_cat-glizigen first instock shipping-taxable purchasable product-type-simple">
	<a href="https://gsaraiva.com/glizigen/product/3x-glizigen-spray-60ml/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-triplo-300x300.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-triplo-300x300.png 300w, https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-triplo-150x150.png 150w, https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-triplo-100x100.png 100w" sizes="(max-width: 300px) 100vw, 300px" /><h2 class="woocommerce-loop-product__title">3x GLIZIGEN SPRAY 60ML</h2>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#82;&#36;</span>350.00</span></span>
</a><a href="/glizigen/shop/?add-to-cart=724" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="724" data-product_sku="" aria-label="Adicionar &ldquo;3x GLIZIGEN SPRAY 60ML&rdquo; no seu carrinho" rel="nofollow">Comprar</a></li>
																	<li class="post-715 product type-product status-publish has-post-thumbnail product_cat-glizigen instock shipping-taxable purchasable product-type-simple">
	<a href="https://gsaraiva.com/glizigen/product/glizigen-spray-60ml/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-unico-1-300x300.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-unico-1-300x300.png 300w, https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-unico-1-150x150.png 150w, https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-unico-1-100x100.png 100w" sizes="(max-width: 300px) 100vw, 300px" /><h2 class="woocommerce-loop-product__title">GLIZIGEN SPRAY 60ML</h2>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#82;&#36;</span>139.98</span></span>
</a><a href="/glizigen/shop/?add-to-cart=715" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="715" data-product_sku="" aria-label="Adicionar &ldquo;GLIZIGEN SPRAY 60ML&rdquo; no seu carrinho" rel="nofollow">Comprar</a></li>
																	<li class="post-722 product type-product status-publish has-post-thumbnail product_cat-glizigen instock shipping-taxable purchasable product-type-simple">
	<a href="https://gsaraiva.com/glizigen/product/glizigen-spray-60ml-1-frasco-50-off/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-duplo-300x300.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-duplo-300x300.png 300w, https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-duplo-150x150.png 150w, https://gsaraiva.com/glizigen/wp-content/uploads/2018/10/glizigen-duplo-100x100.png 100w" sizes="(max-width: 300px) 100vw, 300px" /><h2 class="woocommerce-loop-product__title">GLIZIGEN SPRAY 60ML + 1 FRASCO 50% OFF</h2>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#82;&#36;</span>237.90</span></span>
</a><a href="/glizigen/shop/?add-to-cart=722" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="722" data-product_sku="" aria-label="Adicionar &ldquo;GLIZIGEN SPRAY 60ML + 1 FRASCO 50% OFF&rdquo; no seu carrinho" rel="nofollow">Comprar</a></li>
									
				</ul>

				
			<div class="pagination-wrap"></div>                </div>
                                    <!-- Sidebar -->
                    <div class="col-md-3" id="sidebar-col">
                    	<div id="woocommerce_widget_cart-1" class="widget sidebar-widget woocommerce widget_shopping_cart"><h3 class="widgettitle">Cart</h3><div class="widget_shopping_cart_content"></div></div>                    </div>
                                	</div>
		</div>
	</div>
</div>
    <!-- Site Footer -->
    		<div class="footer-custom-sidebar">
			<div class="container"> 
				<style type="text/css" scoped>
					.vc_custom_1502459762733{padding-top: 30px !important;padding-bottom: 70px !important;}.vc_custom_1502474642401{margin-top: -68px !important;padding-top: 0px !important;}.vc_custom_1502464529966{border-bottom-width: 3px !important;padding-top: 30px !important;padding-right: 30px !important;padding-bottom: 10px !important;padding-left: 30px !important;background-color: #027b7f !important;border-bottom-color: #f8be00 !important;border-bottom-style: solid !important;}.vc_custom_1502464542116{border-bottom-width: 3px !important;padding-top: 30px !important;padding-right: 30px !important;padding-bottom: 10px !important;padding-left: 30px !important;background-color: #027b7f !important;border-bottom-color: #f8be00 !important;border-bottom-style: solid !important;}.vc_custom_1502464553575{border-bottom-width: 3px !important;padding-top: 30px !important;padding-right: 30px !important;padding-bottom: 10px !important;padding-left: 30px !important;background-color: #027b7f !important;border-bottom-color: #f8be00 !important;border-bottom-style: solid !important;}.vc_custom_1540506121528{margin-bottom: 0px !important;}.vc_custom_1540506149023{margin-bottom: 0px !important;}.vc_custom_1540506236518{margin-bottom: 0px !important;}				</style>
				<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid secondary-bg vc_custom_1502459762733"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1502474642401 vc_column-gap-35 vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill"><div class="vc_column-inner vc_custom_1502464529966"><div class="wpb_wrapper"><div class="icon-box  ibox-outline ibox-icon-48 ibox-plain  vc_custom_1540506121528"><div class="ibox-icon secondary-color"><i class="vc-material vc-material-phone_in_talk"></i></div><h3 class="" style="color:#ffffff">0800 87 88 125</h3><p class="" style="color:#eaeaea"> LIGUE GRÁTIS E COMPRE</p></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill"><div class="vc_column-inner vc_custom_1502464542116"><div class="wpb_wrapper"><div class="icon-box  ibox-outline ibox-icon-48 ibox-plain  vc_custom_1540506149023"><div class="ibox-icon secondary-color"><i class="vc-material vc-material-drafts"></i></div><h3 class="" style="color:#ffffff">contato@onpharma.com.br</h3><p class="" style="color:#eaeaea">Entre em contato e tire suas dúvidas</p></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill"><div class="vc_column-inner vc_custom_1502464553575"><div class="wpb_wrapper"><div class="icon-box  ibox-outline ibox-icon-48 ibox-plain  vc_custom_1540506236518"><div class="ibox-icon secondary-color"><i class="vc-material vc-material-chat"></i></div><h3 class="" style="color:#ffffff">19 98259-9144</h3><p class="" style="color:#eaeaea">Entre em contato via WhatsApp</p></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>
			</div>
		</div>
	    		<div class="site-footer site-footer-top footer-light-skin">
			<div class="container">
				<div class="row">
												<div class="col-md-3 col-sm-6 col-xs-12">
															</div>
												<div class="col-md-3 col-sm-6 col-xs-12">
															</div>
												<div class="col-md-3 col-sm-6 col-xs-12">
															</div>
												<div class="col-md-3 col-sm-6 col-xs-12">
															</div>
									</div>
			</div>
		</div>
            <div class="site-footer-bottom footer-light-skin">
    	<div class="container">
        	<div class="row">
            	            		<div class="col-md-6 col-sm-6">
                				                	<div class="copyrights-col-left">
                   		<p>© 2018 Glizigen. Todos Direitos Reservados</p>
                  	</div>
             	                </div>
				            	<div class="col-md-6 col-sm-6">
					                	<div class="copyrights-col-right">
                                                    
                    		<ul class="footer-social imi-social-icons imi-social-icons-rounded imi-social-icons-custom imi-social-icons-bc imi-social-icons-hover-sc">
                           <li class="vimeo"><a href="http://www.vimeo.com" target="_blank"><i class="fa fa-vimeo"></i></a></li><li class="rss"><a href="https://capital.imithemes.com/demo1/feed/" target="_blank"><i class="fa fa-rss"></i></a></li><li class="flickr"><a href="https://www.flickr.com" target="_blank"><i class="fa fa-flickr"></i></a></li>                    	</ul>
                   	</div>
                	           		</div>
               	      		</div>
  		</div>
	</div>
        <a id="back-to-top"><i class="fa fa-angle-up"></i></a> </div>
<!-- End Boxed Body -->
 	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
	<link rel='stylesheet' id='js_composer_front-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.4.7' type='text/css' media='all' />
<link rel='stylesheet' id='vc_material-css'  href='https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/css/lib/vc-material/vc_material.min.css?ver=5.4.7' type='text/css' media='all' />
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-includes/js/admin-bar.min.js?ver=4.9.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/gsaraiva.com\/glizigen\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Verifique se voc\u00ea n\u00e3o \u00e9 um rob\u00f4."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.4'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/glizigen\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/glizigen\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.5.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/glizigen\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/glizigen\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_dbcc5b87d0c7eabcfbf835ff06d0fc04","fragment_name":"wc_fragments_dbcc5b87d0c7eabcfbf835ff06d0fc04"};
/* ]]> */
</script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.5.0'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/themes/capital/js/modernizr.js?ver=1.5.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/themes/capital/vendor/magnific/jquery.magnific-popup.min.js?ver=1.5.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js?ver=5.4.7'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/themes/capital/js/ui-plugins.js?ver=1.5.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/themes/capital/js/helper-plugins.js?ver=1.5.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/themes/capital/vendor/owl-carousel/js/owl.carousel.min.js?ver=1.5.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/lib/bower/flexslider/jquery.flexslider-min.js?ver=5.4.7'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/themes/capital/js/bootstrap.js?ver=1.5.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var imi_local = {"homeurl":"https:\/\/gsaraiva.com\/glizigen\/wp-content\/themes\/capital","sticky_header":"1","siteWidth":"1170","topbar_widgets":"400px"};
/* ]]> */
</script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/themes/capital/js/init.js?ver=1.5.1'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>
<script type='text/javascript' src='https://gsaraiva.com/glizigen/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.4.7'></script>
		<script>	
			jQuery(document).ready(function() {			
				
				if (jQuery('#wp-admin-bar-revslider-default').length>0 && jQuery('.rev_slider_wrapper').length>0) {
					var aliases = new Array();
					jQuery('.rev_slider_wrapper').each(function() {
						aliases.push(jQuery(this).data('alias'));
					});								
					if(aliases.length>0)
						jQuery('#wp-admin-bar-revslider-default li').each(function() {
							var li = jQuery(this),
								t = jQuery.trim(li.find('.ab-item .rs-label').data('alias')); //text()
								
							if (jQuery.inArray(t,aliases)!=-1) {
							} else {
								li.remove();
							}
						});
				} else {
					jQuery('#wp-admin-bar-revslider').remove();
				}
			});
		</script>
			<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Pular para a barra de ferramentas</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Barra de ferramentas" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://gsaraiva.com/glizigen/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">Sobre o WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/about.php">Sobre o WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://br.wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/pt-br:Página_Inicial">Documentação</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://br.wordpress.org/support/">Fóruns de suporte</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://gsaraiva.com/glizigen/wp-admin/">Glizigen</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/">Painel</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/themes.php">Temas</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/widgets.php">Widgets</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/nav-menus.php">Menus</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/customize.php?url=https%3A%2F%2Fgsaraiva.com%2Fglizigen%2Fshop%2F">Personalizar</a>		</li>
		<li id="wp-admin-bar-_options"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/admin.php?page=_options"><span class="ab-icon dashicons-portfolio"></span>Theme Options</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/update-core.php" title="1 atualização de plugin"><span class="ab-icon"></span><span class="ab-label">1</span><span class="screen-reader-text">1 atualização de plugin</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/edit-comments.php"><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-1" aria-hidden="true">1</span><span class="screen-reader-text">1 comentário aguarda moderação</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">Novo</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php">Post</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/media-new.php">Mídia</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=page">Página</a>		</li>
		<li id="wp-admin-bar-new-imi_services"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=imi_services">Service</a>		</li>
		<li id="wp-admin-bar-new-imi_team"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=imi_team">Team</a>		</li>
		<li id="wp-admin-bar-new-imi_projects"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=imi_projects">Project</a>		</li>
		<li id="wp-admin-bar-new-imi_testimonials"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=imi_testimonials">Testimonial</a>		</li>
		<li id="wp-admin-bar-new-imi_custom_sidebar"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=imi_custom_sidebar">VC Sections</a>		</li>
		<li id="wp-admin-bar-new-product"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=product">Produto</a>		</li>
		<li id="wp-admin-bar-new-shop_order"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=shop_order">Pedido</a>		</li>
		<li id="wp-admin-bar-new-shop_coupon"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/post-new.php?post_type=shop_coupon">Cupom</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/user-new.php">Usuário</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-delete-cache"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/index.php?action=delcachepage&#038;path=%2Fshop%2F&#038;_wpnonce=941dc7b8b9" title="Delete cache of the current page">Delete Cache</a>		</li>
		<li id="wp-admin-bar-revslider" class="menupop revslider-menu"><a class="ab-item" aria-haspopup="true" href="https://gsaraiva.com/glizigen/wp-admin/admin.php?page=revslider"><span class="rs-label">Slider Revolution</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-revslider-default" class="ab-submenu">
		<li id="wp-admin-bar-slider1" class="revslider-sub-menu"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/admin.php?page=revslider&#038;view=slide&#038;id=new&#038;slider=1"><span class="rs-label" data-alias="slider1">Slider1</span></a>		</li></ul></div>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="https://gsaraiva.com/glizigen/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Pesquisar</label><input type="submit" class="adminbar-button" value="Pesquisar"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="https://gsaraiva.com/glizigen/wp-admin/profile.php">Olá, <span class="display-name">admin</span><img alt='' src='https://secure.gravatar.com/avatar/f3053411b15f8a1e5837e2c4b03f26b0?s=26&#038;d=mm&#038;r=g' srcset='https://secure.gravatar.com/avatar/f3053411b15f8a1e5837e2c4b03f26b0?s=52&#038;d=mm&#038;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="https://gsaraiva.com/glizigen/wp-admin/profile.php"><img alt='' src='https://secure.gravatar.com/avatar/f3053411b15f8a1e5837e2c4b03f26b0?s=64&#038;d=mm&#038;r=g' srcset='https://secure.gravatar.com/avatar/f3053411b15f8a1e5837e2c4b03f26b0?s=128&#038;d=mm&#038;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>admin</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-admin/profile.php">Editar meu perfil</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="https://gsaraiva.com/glizigen/wp-login.php?action=logout&#038;_wpnonce=6bab2ff337">Sair</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="https://gsaraiva.com/glizigen/wp-login.php?action=logout&#038;_wpnonce=6bab2ff337">Sair</a>
					</div>

		</body>
</html>
<!-- Dynamic page generated in 8.625 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2018-10-25 23:33:56 -->
